import React from 'react';
import ReactDOM from 'react-dom';
import Home from './components/home';
import Link from 'react-router';
import Verify from './components/verify';
import { BrowserRouter, Route, Switch, Redirect, IndexRoute } from 'react-router-dom';
import './styles/style.scss';
import Signup from './components/signup';
import Header from './components/header';
import Profile from './components/profile';
import ForgotPassword from './components/forgotPassword';
import AccountSettings from './components/accountSettings';
import Layout from './components/layout';


class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loggedIn: null
    };
    
    this.appCallback = this.appCallback.bind(this);
  }

  appCallback(childData){
    let self = this;
    console.log('APP CALLBACK');
     console.log(' blerh child data', childData);
     this.setState({loggedIn: childData.loggedIn}, function(){
      console.log('app state', self.state);
      if (self.state.loggedIn){
        console.log(self.context);
      }
     });
  }

  render(){
   return (
    <div>
      <div className="container">
         <BrowserRouter >
            <Layout appCallback={this.appCallback}>
              {/*<Switch>*/}
                <Route path="/" exact render={({props, history}) => <Home {...props} history={history} parentCallback={this.appCallback} loggedIn={this.state.loggedIn}/>}/>
                <Route path="/signup"  render={(props) => <Signup {...props} parentCallback={this.appCallback} loggedIn={this.state.loggedIn}/>}/>
                <Route path="/profile"  render={(props) => <Profile {...props} parentCallback={this.appCallback} loggedIn={this.state.loggedIn}/>}/>
                <Route path="/verify"  render={(props) => <Verify {...props} parentCallback={this.appCallback} loggedIn={this.state.loggedIn}/>}/>
                <Route path="/forgotpassword"  render={(props) => <ForgotPassword {...props} parentCallback={this.appCallback} loggedIn={this.state.loggedIn}/>}/>
                <Route path="/accountsettings"  render={(props) => <AccountSettings {...props} parentCallback={this.appCallback} loggedIn={this.state.loggedIn}/>}/>
              {/*</Switch>*/}
            </Layout>
         </BrowserRouter>
      </div>
   </div>
   )}
}

ReactDOM.render(<App/>, document.getElementById("root"));