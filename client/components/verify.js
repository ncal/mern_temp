import React from 'react';
import $ from 'jquery';
import { Route, Redirect } from 'react-router';
import Foundation from 'react-foundation';
import { Row, Column } from 'react-foundation';

export default class Verify extends React.Component {
   constructor(props) {
      super(props);

      this.formSubmit = this.formSubmit.bind(this);
      
      this.state = {
         signup_message: null,
         redirectToSignup: null
      };
   }

  componentDidMount() {
    console.log('verify mounted');
    // console.log(window.location.search);
    let token = window.location.search.split('');
    token.shift();
    token = token.join('');
    $('.token').val(token);

   if ($('.token').val() !== ''){
     $('.token_submit').click();
   } else {
    this.setState({redirectToSignup: true});
   }
  } 

   formSubmit(e){
      let self = this;
      console.log('token submit');
      e.preventDefault();

      let token = $('.token').val();

      // Validate username and password
      if (token !== ''){
         console.log('token has value');
               $.post('/api/verify', 
                  { token: token }, 
                  function(res){ 
                     console.log(res); 
                     self.setState({'signup_message': res.msg});
                     // if (res.code){$('.verify_form').trigger("reset");}
                  },'json');
      }
   }

  render () {
    if (this.props.loggedIn){
      return  (<Redirect to="/profile" />)
    } 
    if (this.state.redirectToSignup && !this.props.loggedIn){
      return  (<Redirect to="/signup" />)
    }
      return (
        <div >
          <Row className="display verify_container">
            <Column small={10} medium={7} large={5}>
              <p className="verify_msg" style={{textAlign: 'center'}}>Thanks for verifying your account<br/>Please login above</p>
            </Column>
          </Row>
          <form action="/api/verify" className="verify_form" onSubmit={this.formSubmit} style={{display: 'none'}}>
              <input className="token" type="text" name="token" placeholder="token"/>
              <input type="submit" className="token_submit" name="token_submit" value="submit"/>
          </form>
        </div>
      );
  }
}