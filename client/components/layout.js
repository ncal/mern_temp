import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import {Link} from 'react-router-dom';
import $ from 'jquery';
import Header from './header.js';

export default class Layout extends React.Component {
   constructor(props, context) {
      super(props, context);
      this.state = {
         loggedIn: undefined
      };
      let self = this;

      this.myCallback = this.myCallback.bind(this);

      this.styles = {
         width: '100%',
         position: 'absolute', 
         backgroundColor: '#333',
         top: 0,
         left: 0
      };
   }

   componentDidMount() {
      let self = this;
   }  
   componentWillUpdate(nextProps, nextState) {
      // console.log('layout next state', nextState);
   }

   myCallback(dataFromChild){
      let self = this;
      let loggedInFromHeader = dataFromChild.loggedInfromHeader;
      console.log('layout logged In?', loggedInFromHeader);

      self.setState({loggedIn: loggedInFromHeader}, function(){
             console.log('layout state', self.state);
             this.props.appCallback({loggedIn: loggedInFromHeader});
      });
      // return dataFromChild;
   }

   render () {
      // const childrenWithProps = React.Children.map(this.props.children,
      //     (child) => React.cloneElement(child, {
      //       loggedIn: this.state.loggedIn
      //     })
      // );

   return (
      <div>
         <Header parentCallback={this.myCallback}/>
         <div className="body_container">
            {this.props.children}
         </div>
      </div>
      );
  }
}

Header.defaultProps = {
   name: "Layout",
   type: "Child"
}