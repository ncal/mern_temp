import React from 'react';
import $ from "jquery";
import { Redirect } from 'react-router';
import Foundation from 'react-foundation';
import { Row, Column } from 'react-foundation';


export default class AccountSettings extends React.Component {
   constructor(props) {
      super(props);
      console.log('Settings props', props);

      this.reqTest = this.reqTest.bind(this);
      this.welcomeReq = this.welcomeReq.bind(this);

      this.state = {
         data: null,
         user: null,
         loggedIn: this.props.loggedIn
      };
   }

   componentWillMount() {
      console.log('Settings mounted');
      if (this.props.loggedIn){
         this.reqTest();
         this.welcomeReq();
      }
   }

   welcomeReq(){
      let self = this;
      console.log(' welcome req');
      $.getJSON('api/welcome', function(res){
         self.setState({user: res.user_name});
      });
   }

   reqTest(){
      let self = this;
      $.getJSON('api/test', function(res){
         console.log('api/test hit');
         self.setState({data: res});
      });
   }

   render () {
      if (this.props.loggedIn) {
         if (this.state.data !== null){
            return (
               <div>
                  <h1>Account Settings</h1>
                  <p>Hello {this.state.user}</p>

                  {this.state.data.map(function(thing, i){
                     return <p key={i}>{thing.name}</p>;
                  })}
                  <button onClick={this.reqTest} >click me</button>
               </div>
            );

         } else {
            return (
               <div>
                  <h1>test</h1>
                  <button onClick={this.reqTest}>click me</button>
               </div>
            );
         }

      } else {
         return  (<Redirect to="/" />)
      }
   }
}