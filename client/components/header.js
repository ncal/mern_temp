import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import {Link} from 'react-router-dom';
import $ from 'jquery';
import { Route, Redirect } from 'react-router';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
          <form action="/api/login" className="login_form" onSubmit={this.props.login}>
            <input className="username_input" type="text" name="username" placeholder="username"/>
            <input className="password_input" type="password" name="password" placeholder="password"/><br/>
            <input type="submit" name="login" className="login_submit" value="Login"/>
            <span className="login_error" style={{'display': 'none'}}><Link to="/forgotpassword" >Forgot Your Email or Password ?</Link></span>
          </form>
      </div>
    )
  }
}

class Nav extends React.Component {
  render() {
    return (
      <div>
         <ul className="nav">
            {this.props.links.map(function(link, i){
               return <li key={i} className="header_link"> <Link to={link.url} key={i} >{link.name}</Link></li>
            })}
         </ul>
      </div>
    )
  }
}

export default class Header extends React.Component {
   constructor(props, context) {
      super(props, context);
      this.state = {
         loggedIn: false
      };
      this.loggedOutLinks = [{name: 'Home', url: '/'}, 
                           {name: 'Signup', url: '/signup'}];
      this.loggedInLinks = [{name: 'Home', url: '/'}, 
                           {name: 'Profile', url: '/profile'},
                           // {name: 'Settings', url: '/accountsettings'}
                           ];
      let self = this;
      this.login = this.login.bind(this);
      this.logout = this.logout.bind(this);
      this.errorMsg = this.errorMsg.bind(this);
      this.checkLogin = this.checkLogin.bind(this);
   }

   componentWillMount() {
      let self = this;
      console.log(this.props.name+ ' is mounted');
      self.checkLogin();
      console.log('header props', this.props.route);
   }

   checkLogin(){
      console.log('check login');
      let self = this;
      $.get('/api/loggedIn', function(res){
         console.log('loggedIn res', res);
         if (res.loggedIn){
            self.setState({loggedIn: true}, function(){
                self.props.parentCallback({loggedInfromHeader: self.state.loggedIn});
            });
         } else {
            self.setState({loggedIn: false}, function(){
                self.props.parentCallback({loggedInfromHeader: self.state.loggedIn});
            });
         }
      })
   }

   errorMsg(){
      console.log('error message animation');
      $('.login_error').fadeIn('slow');
   }



   login(e){
      let self = this;
      console.log('login');
      e.preventDefault();
      let email = $('.username_input').val();
      let password = $('.password_input').val();

      // Validate username and password
      if (email !== '' && password !== ''){
         // console.log('values checked');
         $.post('/api/login', { email: email, password: password})
            .done(function( data ) {
            // console.log( "Data Loaded: " + data );
            $('.login_form').trigger("reset");
            self.setState({loggedIn: true}, function(){
               console.log('header login work?', self.state.loggedIn);
               self.props.parentCallback({loggedInfromHeader: self.state.loggedIn});
            });
         })
            .fail(function(data) {
               console.log( "failed on client login", data );
               if (data.status === 401){
                  console.log('Wrong Email or password');
                  // show error message to client
                   self.errorMsg();
               }
            })
      }
   }

   logout(){
      let self = this;
      console.log('client logout');
      $.get('/api/logout', function(res){
         // console.log(res);
         self.setState({loggedIn: false}, function(){
            console.log('header login work?', self.state.loggedIn);
            self.props.parentCallback({loggedInfromHeader: self.state.loggedIn});
         });
      });
   }
   render () {
      if (this.state.loggedIn){
         return (
           <div className="header" >
              <Nav links={this.loggedInLinks} />
              <input type="submit" name="logout" className="logout_button" value="Logout"  onClick = {this.logout}/>
           </div>
        );
      } else {
          return (
            <div className="header">
               <LoginForm login={this.login}/>
               <Nav links={this.loggedOutLinks}/>
            </div>
         );
      }
  }
}

Header.defaultProps = {
   name: "Header",
   type: "Child"
}