import React from 'react';
import $ from 'jquery';
import { Route, Redirect } from 'react-router';
import Foundation from 'react-foundation';
import { Row, Column } from 'react-foundation';
export default class ForgotPassword extends React.Component {
   constructor(props) {
      super(props);

      this.formSubmit = this.formSubmit.bind(this);
      this.validateEmail = this.validateEmail.bind(this);
      
      this.state = {
         signup_message: null,
      };
   }

   componentDidMount() {
      let self = this;
      self.setState({loggedIn: self.props.loggedIn});
   }

   validateEmail(email) {
      let emailRegex = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
      return emailRegex.test(email);
   }
   
   formSubmit(e){
      let self = this;
      console.log('forgot password submit');
      e.preventDefault();
      let email = $('.forgot_email_input').val();

      // Validate username and password
      if (email !== ''){
         // console.log('values checked');
            if (this.validateEmail(email)){
               console.log('email is valid');
               $.post('/api/forgotpassword', 
                  { email: email }, 
                  function(res){ 
                     console.log(res); 
                     if (res.msg === 200){
                      console.log('email sent');
                      $('.email_error').fadeOut('slow');
                       $('.forgot_form').fadeOut('slow', function(){
                        $('.forgot_success').fadeIn('slow');
                        $('.forgot_form').trigger("reset");
                      });
                     } else {
                      console.log('email not sent');
                      $('.email_error').fadeIn('slow');
                     }
                  },'json');
            } else {
               alert('email is invalid');
            }
      }
   }

  render () {
   if (this.props.loggedIn){
      return <Redirect to="/profile" />
   } else {
      return (<div>
        <Row className="display forgot_form_container">
          <Column small={10} medium={7} large={5}>
          <p className="forgot_success" style={{textAlign: 'center'}}>Please check your email</p>
          <form action="/api/forgotpassword" className="forgot_form" onSubmit={this.formSubmit}>
            <fieldset>
                <legend>Forgot Password</legend>
                <label >Email</label>
                <input className="forgot_email_input" type="text" name="email" placeholder="email"/>
                <input type="submit" name="forgot_submit" className="forgot_submit"/>
                <span className="email_error"><i> email not recognized</i></span>
              </fieldset>
           </form>
          </Column>
        </Row>
      </div>);
  }
}
}