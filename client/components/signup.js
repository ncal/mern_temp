import React from 'react';
import $ from 'jquery';
import { Route, Redirect } from 'react-router';
import Foundation from 'react-foundation';
import { Row, Column } from 'react-foundation';
export default class signup extends React.Component {
   constructor(props) {
      super(props);

      this.formSubmit = this.formSubmit.bind(this);
      this.validateEmail = this.validateEmail.bind(this);
      
      this.state = {
         signup_message: null,
      };
   }

   componentDidMount() {
      let self = this;
      self.setState({loggedIn: self.props.loggedIn});
   }

   validateEmail(email) {
      let emailRegex = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
      return emailRegex.test(email);
   }
   
   formSubmit(e){
      let self = this;
      console.log('signup submit');
      e.preventDefault();

      let firstname = $('.s_firstname_input').val();
      let lastname = $('.s_lastname_input').val();
      let email = $('.s_email_input').val();
      let password = $('.s_password_input').val();
      let conf = $('.s_conf_input').val();

      // Validate username and password
      if (firstname !== '' && lastname !== '' && email !== '' && password !== '' && conf !== ''){
         // console.log('values checked');
         if (password === conf){
            if (this.validateEmail(email)){
               console.log('email is valid');
               $.post('/api/signup', 
                  { firstname: firstname, lastname: lastname, email: email, password: password, conf: conf}, 
                  function(res){ 
                     // console.log(res); 
                     self.setState({'signup_message': res.msg}, function(){
                        if (self.state.signup_message === 200){$('.signup_form').trigger("reset");}
                     });
                  },'json');
            } else {
               alert('email is invalid');
            }
         } else {
            alert('passwords dont match');
         }
      }
   }

  render () {
   if (this.props.loggedIn){
      return <Redirect to="/profile" />
   } else {
    if (this.state.signup_message !== 200){
      return (<div>
        <Row className="display signup_form_container">
          <Column small={10} medium={7} large={5}>
          <form action="/api/signup" className="signup_form" onSubmit={this.formSubmit}>
            <fieldset>
                <legend>Sign Up</legend>
                <label >First Name</label>
                <input className="s_firstname_input" type="text" name="firstname" placeholder="firstname"/>
                <label >Last Name</label>
                <input className="s_lastname_input" type="text" name="lastname" placeholder="lastname"/><br/>
                <label >Email</label>
                <input className="s_email_input" type="text" name="email" placeholder="email"/>
                <label >Password</label>
                <input className="s_password_input" type="password" name="password" placeholder="password"/><br/>
                <label >Please type your password again</label>
                <input className="s_conf_input" type="password" name="conf" placeholder="confirm"/><br/>
                <input type="submit" name="signup" className="signup_submit"/>
              </fieldset>
           </form>
          </Column>
        </Row>
      </div>);
    } else {
      return (<div>
        <Row className="display signup_form_container">
          <Column small={10} medium={7} large={5}>
          <p className="email_msg" style={{textAlign: 'center'}}>Thanks for signing up<br/> Please check your email</p>
          </Column>
        </Row>
      </div>);
    }
   }
  }
}