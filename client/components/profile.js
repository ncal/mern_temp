import React from 'react';
import $ from "jquery";
import { Redirect } from 'react-router';
import Foundation from 'react-foundation';
import { Row, Column } from 'react-foundation';


export default class Profile extends React.Component {
   constructor(props) {
      super(props);
      console.log('profile props', props);

      this.reqTest = this.reqTest.bind(this);
      this.welcomeReq = this.welcomeReq.bind(this);

      this.state = {
         data: null,
         user: null,
         loggedIn: this.props.loggedIn
      };
   }

   componentWillMount() {
      let self = this;
      console.log('profile mounted');
      if (this.props.loggedIn){
         self.setState({loggedIn: true});
         this.reqTest();
         this.welcomeReq();
      }
   }

   welcomeReq(){
      let self = this;
      console.log(' welcome req');
      $.getJSON('api/welcome', function(res){
         self.setState({user: res.user_name});
      });
   }

   reqTest(){
      let self = this;
      $.getJSON('api/test', function(res){
         console.log('api/test hit');
         self.setState({data: res});
      });
   }

   render () {
      if (!this.props.loggedIn) {
         return (<Redirect to="/" />);
      } else {
         if (this.state.data !== null){
            return (
               <div>
                  <h1>Profile</h1>
                  <h>Hello {this.state.user}</h>
                  {this.state.data.map(function(thing, i){
                     return <p key={i}>{thing.name}</p>;
                  })}
                  <button onClick={this.reqTest} >click me</button>
               </div>
            );
         } else {
            return (
               <div>
                  <div>Please Login to view this page</div>
               </div>
            );
         }
      }
   }
}