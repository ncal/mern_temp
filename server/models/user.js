var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var UserSchema = new mongoose.Schema({
    active: {
        type: Boolean,
        unique: false,
        required: true
    },
    token: {
        type: String,
        unique: false,
        trim: true
    },
    firstname: {
        type: String,
        unique: false,
        required: true,
        trim: true
    },
    lastname: {
        type: String,
        unique: false,
        required: true,
        trim: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        unique: false,
        required: true,
        trim: true
    },
    ip: {
        type: String,
        unique: false,
        required: true,
        trim: true
    }
});


// authenticate user input against db documents
UserSchema.statics.authenticate = function(email, password, callback) {
    console.log('in authenticate');
    user.findOne({ email: email })
    .exec(function(error, user){
      console.log('executing');
      if (error){
          console.log('error finding user');
          callback(error, null);
        } 
        if (!user || user === null) {
          console.log('user is null');
          error = new Error('user not found');
          error.status = 401;
          callback(error, null);
        } else {
          console.log('user found');
          console.log(user);
          console.log('Authenticating!');
          bcrypt.compare(password, user.password, function(err, result){
            console.log('result', result);
            if (result){
              console.log('ITS TRUE');
              return callback(null, user);
            } else {
              return callback();
            }
          });
        }
      });

};

function handleError(error){
  console.log(error);
    return error;
}

// hash password before storing it in the user db
UserSchema.pre('save', function(next) {
    var user = this;
    bcrypt.hash(user.password, 10, function(err, hash) {
        console.log('hashing');
        if (err) {
            console.log('error hashing password');
        } else {
            console.log('password has been hashed');
            user.password = hash;
            next();
        }
    });
});

var user = mongoose.model('user', UserSchema);
module.exports = user;