let deps = require('../misc/deps.js');
let router = deps.router;
let mongoose = deps.mongoose;
let path = deps.path;
let user = deps.user;
let randomstring = deps.randomstring;
let mailer = deps.mailer;

let signup = router.post('/api/signup', function(req, res, next) {
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
   console.log('req body', req.body);

    if (req.body.firstname && req.body.lastname && req.body.email && req.body.password && req.body.conf) {
        if (req.body.password !== req.body.conf) {
            console.log('passwords dont match');
            let err = new Error('passwords do not match');
            err.status = 400;
            return next(err);
        }
        let ip = req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress;

        let sign = {
            active: false,
            token : randomstring.generate(),
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            password: req.body.password,
            ip: ip
        };
        console.log('signup', sign);

        // push user into database
        user.create(sign, function(err, result) {
            if (err) {
             console.log(err);
             if (err.code === 11000) {
                 err = new Error('User already exists');
                 err.status = 400;
                 console.log('user already exists');
                 return next(err);

             } else {
                 console.log('error creating user');
                 err = new Error('error creating user');
                 err.status = 400;
                 return next(err);

             }
            } else {
                // SEND EMAIL
                let html = `hi there <br/> thank you for registering <br/> please click <a href="http://localhost:3000/verify?${result.token}">this link</a> to verify your account <br/>`;
                mailer.sendEmail('admin@nbtest.com', sign.email, 'please verify your account', html);
                return res.json({msg: 200, 'detail': 'check your email'});
            }
            db.close();
        });
    }
});

module.exports = signup;
