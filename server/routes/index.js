let deps = require('../misc/deps.js');
let router = deps.router;
let mongoose = deps.mongoose;
let grid = deps.grid;
let path = deps.path;
let bluebird = deps.bluebird;
mongoose.Promise = bluebird;

mongoose.connect("mongodb://localhost:27017/mern", {
    server: {
        poolSize: 1, /// <--pool connections 
        socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 }
    },
    replset: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } },
    useMongoClient: true,
    promiseLibrary: require('bluebird')
});

let conn = mongoose.connection;
grid.mongo = mongoose.mongo;

require('./login.js');
require('./test.js');
require('./loggedIn.js');
require('./welcome.js');
require('./logout.js');
require('./signup.js');
require('./verify.js');
require('./forgotPassword.js');
// require('./accountSettings.js');

router.get('*', function (req, res) {
  res.sendFile(path.join(__dirname, '../../client/index.html'));
});


module.exports = router;