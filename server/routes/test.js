let deps = require('../misc/deps.js');
let router = deps.router;
let mongoose = deps.mongoose;
let mid = deps.mid;

let test = router.get('/api/test', mid.requiresLogin, function (req, res, next) {
   let db = mongoose.connection;
   db.on('error', console.error.bind(console, 'connection error'));

   console.log('test working');

   mongoose.connection.db.collection('test', function(error, collection) {
    if (error){console.error('could not connect to test collection');} else {
      collection.find({}).toArray(function(err, result) {
          if (err) {
              console.error('error getting data from test', err);
              err = new Error('error getting data from test');
              err.status = 500;
              return next(err);
          } else if (result.length) {
           console.log(result);
           res.send(result);
          } else {
              res.send('no documents found');
          }
      });
    }
   });
});

module.exports = test;