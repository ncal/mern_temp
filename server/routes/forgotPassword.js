let deps = require('../misc/deps.js');
let router = deps.router;
let mongoose = deps.mongoose;
let path = deps.path;
let user = deps.user;
let randomstring = deps.randomstring;
let mailer = deps.mailer;

let forgotPassword = router.post('/api/forgotpassword', function(req, res, next) {
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    console.log('req body', req.body);
    
    let newPass = randomstring.generate({length: 8});
    //if email exists
    if (req.body.email) {
        // find the user with this email
        user.findOne({email: req.body.email}, function (error, user) {

            if (error || user === null){
                console.error('user email not found in DB');
                // let notFound = new Error('user email not found in DB');
                return res.send({msg: 400, detail: 'user email not found in db'});
            } else {
                user.password = newPass;
                user.save(function (err) {
                    if(err) {
                        console.error('ERROR!');
                    }
                    let html = `hi there <br/> Your new password is ${newPass} <br/> please click <a href="http://localhost:3000/">this link</a> to login<br/>`;
                    mailer.sendEmail('admin@nbtest.com', req.body.email, 'Password Reset', html);
                    return res.json({msg: 200, 'detail': 'check your email'});
                });
            }
        });
    }
            // db.close();
});

module.exports = forgotPassword;
