let deps = require('../misc/deps.js');
let router = deps.router;
let mongoose = deps.mongoose;
let user = deps.user;

// // POST TO THE DB via mongoose schema (into the user collection)
let login = router.post('/api/login', function(req, res, next) {
    console.log(req.body);
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));


    if (req.body.email && req.body.password) { 
      let err;
        user.authenticate(req.body.email, req.body.password, function(error, user) {
            if (user === null){
               err = new Error('user does not exist');
               err.status = 401;
               return next(err);
            }
            if (error || !user) {
               err = new Error('wrong email or password');
               err.status = 401;
               return next(err);
            } else if (!user.active) {
               console.log('not verified');
               err = new Error('please verify your email');
               err.status = 401;
               return next(err);
            } else {
               console.log('assigning session variables');
               req.session.userId = user._id;
               req.session.email = user.email;
               req.session.password = user.password;

               return res.json({login_msg: 200});

            }
            db.close();
        });
    }
});

module.exports = login;