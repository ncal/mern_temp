let deps = require('../misc/deps.js');
let router = deps.router;
let mongoose = deps.mongoose;
let mid = deps.mid;
let user = deps.user;

let welcome = router.get('/api/welcome', mid.requiresLogin,  function (req, res, next) {
   let db = mongoose.connection;
   db.on('error', console.error.bind(console, 'connection error'));

   console.log('welcome working');
   user.findById(req.session.userId)
        .exec(function(error, user) {
            if (error) {
                error = new Error('Error finding user in *Welcome*');
                return next(error);
            } else {
                console.log('*welcome* user found', user);
              if (user === null) { 
                return next(error);
              } else {
                return res.send({user_email: user.email, user_name: user.firstname});
              }
            }
        });
});

module.exports = welcome;