let deps = require('../misc/deps.js');
let router = deps.router;
let mongoose = deps.mongoose;
let mid = deps.mid;
let user = deps.user;

let verify = router.post('/api/verify', function(req, res, next) {
    console.log(req.body);
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));

    user.findOneAndUpdate({ token: req.body.token }, { $set: { active: true, token: '' } },
        function(err, doc) {
            if (err) {
                console.error('error updating user status in db', err);
                err = new Error('error updating user status in db');
                err.status = 500;
                return next(err);
            } else {
                console.log('user with token', doc);
            }
            return res.send({ msg: 200, detail: 'user status updated' });
        });
});

module.exports = verify;