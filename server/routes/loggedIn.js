let deps = require('../misc/deps.js');
let router = deps.router;
let mongoose = deps.mongoose;

let loggedIn = router.get('/api/loggedIn', function (req, res, next) {
   let db = mongoose.connection;
   db.on('error', console.error.bind(console, 'connection error'));
   req.session.userId === undefined ? res.send({loggedIn: false}) : res.send({loggedIn: true});
});

module.exports = loggedIn;