let deps = require('../misc/deps.js');
let router = deps.router;
let mongoose = deps.mongoose;
let mid = deps.mid;

let logout = router.get('/api/logout', mid.requiresLogin, function (req, res, next) {
  console.log('logout');
    if (req.session) {
        console.log('destroying the session');
        // delete session object
        req.session.destroy(function(err) {
            if (err) {
                return next(err);
            } else {
                res.send({msg: 200, detail: 'logout successful'});
            }
        });
    }
});

module.exports = logout;