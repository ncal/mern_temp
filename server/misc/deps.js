var deps = {
    router: require('express').Router(),
    mongodb: require('mongodb'),
    mongoose: require('mongoose'),
    formidable: require('formidable'),
    grid: require('gridfs-stream'),
    mid: require('../middleware'),
    fs: require('fs'),
    path: require('path'),
    user: require('../models/user'), // these models become collections and are pluralized ie: user:> users
    nev: require('email-verification')(this.mongoose),
    randomstring: require('randomstring'),
    mailer: require('../misc/mailer'),
    bluebird: require('bluebird'),
    middleware: require('../middleware/index.js')
};

module.exports = deps;