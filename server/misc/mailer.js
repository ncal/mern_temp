var nodemailer = require('nodemailer');

var transport = nodemailer.createTransport({
   service: 'Mailgun',
   auth: {
      user: 'postmaster@sandbox887ba6df47c64d5fbd4dd6938f7f465b.mailgun.org',
      pass: 'aa827bf6cb011c5e51bceb70652f7dda'
   },
   tls: {
      rejectUnauthorized: false
   }
});

module.exports= {
   sendEmail(from, to, subject, html){
      return new Promise((resolve, reject) => {
         transport.sendMail({ from, subject, to, html}, (err, info) =>{
            if (err) reject (err);
            resolve(info);
         });
      });
   }
}