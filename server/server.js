// PACKAGES //
var path = require('path');
var fs = require('fs');
var express = require('express');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser'); 
var session = require('express-session');
var MongoStore = require('connect-mongo')(session); //let connect mongo middleware access the sessions
var mongoose = require('mongoose');
var bluebird = require('bluebird');
mongoose.Promise = bluebird;
// IMPORTS //
var indexRoutes = require('./routes/index');

// CREATE APP //
var app = express();

// VIEW ENGINE //
app.set('view engine', 'html');
app.engine('html', function (path, options, callbacks) {
  fs.readFile(path, 'utf-8', callback);
});
app.set('json spaces', 0);

// MIDDLEWARE //
app.use(express.static(path.join(__dirname, '../client')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))
.use(session({
   secret: 'terry flap',
   resave: true,
   saveUninitialized: false,
   ip:  mongoose.connection.remoteAddress,
   store: new MongoStore({
      mongooseConnection: mongoose.connection //create a sessions collection 
   })
}));

// make user id available in templates
app.use(function(req, res, next){
  res.locals.currentUser = req.session.userId;
  next();
});

// ROUTES //
app.use('/', indexRoutes);

// ERROR HANDLER //
app.use(function (err, req, res, next) {
  console.log(err.stack);
  res.status(err.status || 500).send('something broke');
});

var port = 3000;
app.listen(port, function(){
   console.log('running at localhost:' + port);
});